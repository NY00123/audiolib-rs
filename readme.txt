                             Basic sound recorder

                               20200111 release

           A modification of Simple sound player from James R. Dose.

This program was originally made by modifying Simple sound player.
The player was written in order to let a user of it test a sound file
using the Apogee Sound System. The recorder was made in order to check
if it could be used in a virtual or emulated machine offering
some form of Sound Blaster emulation.

As of the 20200111 release, there aren't knowingly many such environments
which support audio recording from the host environment for the emulated SB.

The recorder was also modified in order to be compatible with the
Apogee Sound System Version 1.12, as open-sourced in December 2002.
In particular, this means that the program should theoretically
work with a Sound Blaster or compatible while using this version of the
sound system, if the BLASTER environment variable is appropriately set.

Please note that this program is being offered without any kind
of support. It may malfunction in any non-deterministic way.
Use at your own risk.

------

In order to use the program, you need a compatible
DOS extender, like DOS/4GW. Once the extender is ready for use,
start "RS" with no arguments. It should then show how to use it.

------

This program is licensed under the terms of the GNU GPL
(General Public License) version 2 or later. See gpl.txt for the details.

Additionally, you are granted a linking exception, for the purpose of linking
the code present in the program with the Build Engine and other code used in
the original Duke Nukem 3D and Shadow Warrior EXEs from 1990-2000. This also
covers derivatives of the same code, like modern ports of the Build Engine.

The Apogee Sound System (AUDIO_WF.LIB) was open sourced in December 2002,
and the same terms should apply to it, at least since being included
with the Duke Nukem 3D sources as released in April 2003.

As of January 11th 2020, there should be multiple options for obtaining
these Apogee Sound System sources, links being given here:

https://github.com/videogamepreservation/rott
https://github.com/videogamepreservation/dukenukem3d
https://github.com/dos-games/vanilla-rott
https://github.com/dos-games/vanilla-shadow_warrior
https://dukeworld.duke4.net/2001-current/source/
-> rottsource.zip, duke3dsource.zip and shadowwarriorsource.zip.
ftp://ftp.3drealms.com/source/
-> rottsource.zip, duke3dsource.zip and shadowwarriorsource.zip.

-Yoav N.
